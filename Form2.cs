﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1
{
    public partial class Form2 : Form
    {
        private string username;

        //this is the c'tor of the form
        public Form2(string username)
        {
            InitializeComponent();
            this.username = username;
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {

        }

        /*
        this function take care for the option if someone press on a date
        input:  sender
                e
        output: none
        */
        private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
            string fullTime = e.End.Date.ToString(); //get the date
            string[] times = fullTime.Split(' ');    //split all the dates
            string[] dayMonthYear = times[0].Split('/');

            //orginize the date as show in the txt files
            string orginizedDate = dayMonthYear[1] + '/' + dayMonthYear[0] + '/' + dayMonthYear[2];
            //remove all the zeros
            orginizedDate = orginizedDate.Replace("0", "");
            string temp = (this.username + "BD.txt");

            //read all the names
            string[] usersDetails = File.ReadAllLines(temp);

            int i = 0;

            //run on all the names
            for(i = 0; i < usersDetails.Length; i++)
            {
                string str = (usersDetails[i].Split(','))[1];
                str = str.Replace("0", "");
                
                //if the date is same, print message
                if (str.Equals(orginizedDate))
                {
                    this.label1.Text = "בתאריך הנבחר - " + (usersDetails[i].Split(','))[0] + " חוגג יום הולדת!";
                    break;
                }
                //else print other message
                else
                {
                    this.label1.Text = "בתאריך הנבחר - איש לא חוגג יומולדת";
                }
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
