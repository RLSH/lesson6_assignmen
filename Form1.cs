﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace _1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void userLabel_Click(object sender, EventArgs e)
        {

        }

        private void cancel_Click(object sender, EventArgs e)
        {
            //if the user pressed cancel, shut doen the program
            Environment.Exit(100);
        }

        private void userBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void passBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void passLabel_Click(object sender, EventArgs e)
        {

        }

        private void enter_Click(object sender, EventArgs e)
        {
            // all the users is here
            string[] usersDetails = File.ReadAllLines("Users.txt");
            int i = 0;
            int j = 0;
            string allLine = "";
            string username = "";
            string password = "";

            
            for(i = 0; i < usersDetails.Length; i++)
            {
                allLine = usersDetails[i];

                j = 0;

                //find out what is the username and what is the password
                while(j < allLine.Length && allLine[j] != ',')
                {
                    username += allLine[j];
                    j++;
                }

                j++;
                while (j < allLine.Length)
                {
                    password += allLine[j];
                    j++;
                }

                //if the username and the password are correct by the file input,
                if(userBox.Text.Equals(username) && passBox.Text.Equals(password))
                {
                    //go to the other form
                    this.label1.Text = "";
                    Form2 f = new Form2(username);
                    this.Hide();
                    f.ShowDialog();
                    this.Show();

                    break;
                }
                else
                {
                    //else print message
                    this.label1.Text = "error: can't find username or password";
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
