﻿namespace _1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.userBox = new System.Windows.Forms.TextBox();
            this.passBox = new System.Windows.Forms.TextBox();
            this.userLabel = new System.Windows.Forms.Label();
            this.passLabel = new System.Windows.Forms.Label();
            this.enter = new System.Windows.Forms.Button();
            this.cancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // userBox
            // 
            this.userBox.Location = new System.Drawing.Point(12, 77);
            this.userBox.Name = "userBox";
            this.userBox.Size = new System.Drawing.Size(114, 20);
            this.userBox.TabIndex = 0;
            this.userBox.TextChanged += new System.EventHandler(this.userBox_TextChanged);
            // 
            // passBox
            // 
            this.passBox.Location = new System.Drawing.Point(12, 150);
            this.passBox.Name = "passBox";
            this.passBox.Size = new System.Drawing.Size(114, 20);
            this.passBox.TabIndex = 1;
            this.passBox.TextChanged += new System.EventHandler(this.passBox_TextChanged);
            // 
            // userLabel
            // 
            this.userLabel.AutoSize = true;
            this.userLabel.Location = new System.Drawing.Point(177, 80);
            this.userLabel.Name = "userLabel";
            this.userLabel.Size = new System.Drawing.Size(68, 13);
            this.userLabel.TabIndex = 2;
            this.userLabel.Text = ":שם משתמש";
            this.userLabel.Click += new System.EventHandler(this.userLabel_Click);
            // 
            // passLabel
            // 
            this.passLabel.AutoSize = true;
            this.passLabel.Location = new System.Drawing.Point(201, 153);
            this.passLabel.Name = "passLabel";
            this.passLabel.Size = new System.Drawing.Size(44, 13);
            this.passLabel.TabIndex = 3;
            this.passLabel.Text = ":סיסמא";
            this.passLabel.Click += new System.EventHandler(this.passLabel_Click);
            // 
            // enter
            // 
            this.enter.Location = new System.Drawing.Point(12, 209);
            this.enter.Name = "enter";
            this.enter.Size = new System.Drawing.Size(114, 23);
            this.enter.TabIndex = 4;
            this.enter.Text = "כניסה";
            this.enter.UseVisualStyleBackColor = true;
            this.enter.Click += new System.EventHandler(this.enter_Click);
            // 
            // cancel
            // 
            this.cancel.Location = new System.Drawing.Point(132, 209);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(113, 23);
            this.cancel.TabIndex = 5;
            this.cancel.Text = "ביטול";
            this.cancel.UseVisualStyleBackColor = true;
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 6;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.enter);
            this.Controls.Add(this.passLabel);
            this.Controls.Add(this.userLabel);
            this.Controls.Add(this.passBox);
            this.Controls.Add(this.userBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox userBox;
        private System.Windows.Forms.TextBox passBox;
        private System.Windows.Forms.Label userLabel;
        private System.Windows.Forms.Label passLabel;
        private System.Windows.Forms.Button enter;
        private System.Windows.Forms.Button cancel;
        private System.Windows.Forms.Label label1;
    }
}

